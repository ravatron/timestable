# -cd timestable
# -git remote update
# -git pull
# then start coding

import random


mult = False
print("Woule you like to do simple timestables or advanced timestables?")
opt = input("simple(s) or advanced(a) >>> ")
if opt.lower() == "s" or opt.lower() == "simple":
    cont = True
    while cont == True:
# get user input and convert to int
        if mult == False:
            num = int(input("Please enter a times table you would like to practice: "))

# get 10 random numbers from 1-12
        tab = range(1, 13)
        amount = int(input("Please enter the number of practice questions you would like: "))
        quest = random.sample(tab, amount)

# Have user answer multiplication questions in random order and give feedback
        for q in quest:
            print("What is {0} * {1} equal to?".format(num, q))
            ans = int(input(">>> "))
            if ans == num * q:
                print("Congrats, that is correct!")
            else:
                print("Sorry, {0} is not what {1} * {2} equals.".format(ans, num, q))
                print("{0} * {1} = {2}".format(num, q, num * q))

# ask user if they want to continue w/ same number, or different number, or quit
        print("Would you like to continue with this table or try a different one?")
        v = 0
        while v == 0:
            ans = input("same(s) or different(d) or quit(q) >>> ")
            if ans.lower() == "same" or ans.lower() == "s":
                cont = True
                mult = True
                v = 1
            elif ans.lower() == "different" or ans.lower() == "d":
                cont = True
                mult = False
                v = 1
            elif ans.lower() == "quit" or ans.lower() == "q":
                cont = False
                v = 1
                print("Thanks for playing!!!!!")
            else:
                v = 0
                print("That didn't work, make sure you're spelling is correct!")

elif opt.lower() == "advanced" or opt.lower() == "a":
    cont = True
    while cont == True:
        while mult == False:
            num = int(input("Please enter a times table you would like to practice (1-10): "))
            if num > 10 or num < 1:
                print("Sorry, enter a number between 1 and 10")

            else:
                mult = True
        tab = range(11, 100)
        amount = int(input("Please enter the number of practice questions you would like: "))
        quest = random.sample(tab, amount)

        for q in quest:
            print("Your timestable is {0} * {1}".format(num, q))
            a = str(q)
            print("What is {0} * {1}0?".format(num, a[0]))
            ans1 = int(input(">>> "))
            if ans1 != num * int(a[0]) * 10:
                print("Sorry, {0} * {1}0 does not equal {2}".format(num, a[0], ans1))
                print("{0} * {1}0 = {2}".format(num, a[0], int(a[0]) * 10 * num))
            elif ans1 == num * int(a[0]) * 10:
                print("Congrats, that is correct!")
            print("Now what is {0} * {1}?".format(num, a[1]))
            ans2 = int(input(">>> "))
            if ans2 != num * int(a[1]):
                print("Sorry, {0} * {1} does not equal {2}".format(num, a[1], ans2))
                print("{0} * {1} = {2}".format(num, a[1], int(a[1]) * num))
            elif ans2 == num * int(a[1]):
                print("Congrats, that is correct!")
            print("Now what is {0} + {1}?".format(num * 10 * int(a[0]), num * int(a[1])))
            ans = int(input(">>> "))
            if ans == num * q:
                print("Congrats, that is correct!")
            else:
                print("Sorry, {0} * {1} does not equal {2}".format(num, q, ans))
                print("{0} * {1} = {2}".format(num, q, num * q))


        print("Would you like to continue with this table or try a different one?")
        v = 0
        while v == 0:
            ans = input("same(s) or different(d) or quit(q) >>> ")
            if ans.lower() == "same" or ans.lower() == "s":
                cont = True
                mult = True
                v = 1
            elif ans.lower() == "different" or ans.lower() == "d":
                cont = True
                mult = False
                v = 1
            elif ans.lower() == "quit" or ans.lower() == "q":
                cont = False
                v = 1
                print("Thanks for playing!!!!!")
            else:
                v = 0
                print("That didn't work, make sure you're spelling is correct!")
